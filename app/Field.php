<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FieldProcessed;
use App\User;

class Field extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'area', 'user_id', 'type_id'
    ];


    public function fieldProcessed()
    {
        return $this->hasMany(FieldProcessed::class, 'field_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
