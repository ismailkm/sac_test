<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Field;
use App\Tractor;
use App\User;

class FieldProcessed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'tractor_id', 'field_id', 'user_id', 'proc_date','area'
    ];


    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }

    public function tractor()
    {
        return $this->belongsTo(Tractor::class, 'tractor_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
