<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Field;
use App\Traits\ApiResponse;

class FieldsController extends Controller
{
    use ApiResponse;
    /*
    *
    */
    public function addField(Request $request)
    {
        $this->validate($request, [
          'name'    => 'required',
          'area'    => 'required',
          'user_id' => ['required', 'exists:users,id'],
          'type_id' => ['required', 'exists:field_types,id'],
        ]);

        $data = $request->only(['name', 'area', 'user_id', 'type_id']);
        $field = Field::create($data);
        if ($field) {
            return $this->response(compact('field'));
        } else {
            return $this->response([], 500, false, "Field is not added. Please try again.");
        }
    }
}
