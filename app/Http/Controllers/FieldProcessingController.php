<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\FieldProcessed;
use App\Field;
use App\User;
use App\Traits\ApiResponse;

class FieldProcessingController extends Controller
{
    use ApiResponse;

    /*
    *
    */
    public function addFieldProcessing(Request $request)
    {
        $this->validate($request, [
          'tractor_id' => ['required', 'exists:tractors,id'],
          'field_id' => ['required', 'exists:fields,id'],
          'user_id' => ['required', 'exists:users,id'],
          'proc_date' => 'required|date_format:Y-m-d',
          'area' => 'required',
        ]);

        $data = $request->only(['tractor_id', 'field_id', 'user_id', 'proc_date','area']);


        $prevouse_processed = $this->getProcessedArea($data);

        $field = Field::find($data['field_id']);

        if ($prevouse_processed > $field->area) {
            return $this->response([], 500, false, "Process area can not be more then actual field Area " . $field->area . ".");
        } else {
            $proccessing = FieldProcessed::create($data);
            if ($proccessing) {
                return $this->response(compact('proccessing'));
            } else {
                return $this->response([], 500, false, "Field Processing is not added. Please try again.");
            }
        }
    }


    public function getProcessingReport(Request $request)
    {
        $proccessings = FieldProcessed::with('field', 'tractor', 'user');

        $proccessings->whereHas('field', function ($query) use ($request) {
            if ($request->field) {
                $query->where('name', 'like', "%{$request->field}%");
            }
        });

        $proccessings->whereHas('tractor', function ($query) use ($request) {
            if ($request->tractor) {
                $query->where('name', 'like', "%{$request->tractor}%");
            }
        });

        if ($request->proc_date) {
            $proccessings->where('proc_date', '=', Carbon::parse($request->proc_date)->format("Y-m-d"));
        }

        $response = $proccessings->get();

        return $this->response(compact('response'));
    }

    protected function getProcessedArea($data)
    {
        $prevouse_processed = FieldProcessed::where('field_id', '=', $data['field_id'])
                            ->sum('area');

        return $prevouse_processed + $data['area'];
    }
}
