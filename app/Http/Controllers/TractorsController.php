<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tractor;
use App\Traits\ApiResponse;

class TractorsController extends Controller
{
    use ApiResponse;
    /*
    *
    */
    public function addTractor(Request $request)
    {
        $this->validate($request, [
          'name'    => 'required',
        ]);

        $data = $request->only(['name']);
        $tractor = Tractor::create($data);
        if ($tractor) {
            return $this->response(compact('tractor'));
        } else {
            return $this->response([], 500, false, "Tractor is not added. Please try again.");
        }
    }
}
