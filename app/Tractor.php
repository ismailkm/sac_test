<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FieldProcessed;

class Tractor extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name'
    ];


    public function fieldProcessed()
    {
        return $this->hasMany(FieldProcessed::class, 'tractor_id');
    }
}
