<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

trait ApiResponse
{
    public function response($data = [], $code = Response::HTTP_OK, $success = true, $message = '')
    {
        return response()->json(
            [
            'success' => $success,
            'message' => $message,
            'data'    => $data,
          ],
            $code
        );
    }
}
