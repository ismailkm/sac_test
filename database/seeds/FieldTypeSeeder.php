<?php

use Illuminate\Database\Seeder;

class FieldTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('field_types')->insert([
            'name' => 'Wheat'
        ]);

        DB::table('field_types')->insert([
            'name' => 'Broccoli'
        ]);

        DB::table('field_types')->insert([
            'name' => 'Strawberries'
        ]);
    }
}
