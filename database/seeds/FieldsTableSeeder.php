<?php

use Illuminate\Database\Seeder;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fields')->insert([
          'name' => 'test',
          'area' => 20,
          'user_id' => 1,
          'type_id' => 1,
       ]);
    }
}
