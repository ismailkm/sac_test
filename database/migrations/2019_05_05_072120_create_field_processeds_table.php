<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldProcessedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_processeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('proc_date');
            $table->float('area', 8, 2);
            $table->integer('field_id');
            $table->integer('tractor_id');
            $table->integer('user_id');
            $table->timestamps();

            // $table->foreign('field_id')->references('id')->on('fields')->onDelete('cascade');
            // $table->foreign('tractor_id')->references('id')->on('tractors')->onDelete('cascade');
            // $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_processeds');
    }
}
