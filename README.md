# Agriculture

Test is build on Lumne micro framework.

## Prerequisite


```bash
PHP 7, MYSQL, Apache, laravel/lumen,
```

## Database

```bash
Please import provided database, with the project, in mysql.
```

## Installation

Use the package manager composer to install foobar.

```bash
composer install
php artisan migrate
 -- with seeder
php artisan migrate --seed
```

## APIs
for the parameters, please check the postman json provided.

Add field

```bash
api/fields
```
Add Tractor

```bash
/api/tractor
```
Add Field processing

It will get the area of fields from previous processing entries and add provided one with it and then compare with the area of the field.

```bash
/api/processing
```

Processing report



```bash
/api/report
```
